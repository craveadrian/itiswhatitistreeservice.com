<div id="topService">
	<div class="row">
		<dl>
			<dt> <img src="public/images/content/topservice1.jpg" alt="residential"> </dt>
			<dd>
				<h5>RESIDENTIAL</h5>
				<p>Finding the right painting contractor can make all the difference in the world for your home improvement project. The color, even the specific shade, will affect how people see both your home and you.</p>
				<a href="service#content" class="btn">READ MORE</a>
			</dd>
		</dl>
		<dl>
			<dt> <img src="public/images/content/topservice2.jpg" alt="commercial"> </dt>
			<dd>
				<h5>COMMERCIAL</h5>
				<p>There is nothing like a reliable, all-purpose home remodeling expert to help you achieve your ideal building design. It Is What It Is Tree Service prides itself on its reliability and superior customer service. </p>
				<a href="service#content" class="btn">READ MORE</a>
			</dd>
		</dl>
	</div>
</div>
<div id="botService">
	<div class="row">
		<h2>OUR SERVICES</h2>
		<div class="container">
			<a href="services#content"><dl>
				<dt> <img src="public/images/content/service1.jpg" alt="Service 1"> </dt>
				<dd> <h5>LANDSCAPING</h5> </dd>
			</dl></a>
			<a href="services#content"><dl>
				<dt> <img src="public/images/content/service2.jpg" alt="Service 2"> </dt>
				<dd> <h5>PAINTING SERVICES</h5> </dd>
			</dl></a>
			<a href="services#content"><dl>
				<dt> <img src="public/images/content/service3.jpg" alt="Service 3"> </dt>
				<dd> <h5>REMODELING SERVICES</h5> </dd>
			</dl></a>
			<a href="services#content"><dl>
				<dt> <img src="public/images/content/service4.jpg" alt="Service 4"> </dt>
				<dd> <h5>HVAC</h5> </dd>
			</dl></a>
			<a href="services#content"><dl>
				<dt> <img src="public/images/content/service5.jpg" alt="Service 5"> </dt>
				<dd> <h5>PLUMBING</h5> </dd>
			</dl></a>
			<a href="services#content"><dl>
				<dt> <img src="public/images/content/service6.jpg" alt="Service 6"> </dt>
				<dd> <h5>PRESSURE WASHING</h5> </dd>
			</dl></a>
		</div>
	</div>
</div>
<div id="review">
	<div class="row">
		<h2>CLIENT REVIEWS</h2>
		<img src="public/images/common/tools.png" alt="tools" class="tools">
		<div class="container">
			<dl>
				<dt>&#9733; &#9733; &#9733; &#9733; &#9733;</dt>
				<dd>I wanted to write to you regarding service that your company performed at my home. The crew arrived on time, got right to work, responded to my inquiries and requests, completed the job in a timely fashion and did a great job of cleaning up our property, as well as our two neighbors. Additionally, they were all pleasant, courteous and very respectful of our property</dd>
			</dl>
			<dl>
				<dt>&#9733; &#9733; &#9733; &#9733; &#9733;</dt>
				<dd>These guys did a great job removing two trees that on my property. They were punctual, professional, and had the best quote of all that gave an estimate. They didn’t quit until they got evey bit of debris cleaned up. I highly recommend them.</dd>
			</dl>
			<dl>
				<dt>&#9733; &#9733; &#9733; &#9733; &#9733;</dt>
				<dd>Paul  and his crew were at my home today to trim our trees and we have nothing but praise for them and your operation. Thank you for such quality! Not only did they do an excellent job trimming but, cleaned up after the job was complete.   </dd>
			</dl>
		</div>
		<a href="testimonials#content" class="btn">READ MORE</a>
	</div>
</div>
<div id="why">
	<h2>WHY CHOOSE US</h2>
	<img src="public/images/common/tree.png" alt="Tree" class="tree">
	<div class="container">
		<dl>
			<dt> <img src="public/images/content/why1.jpg" alt="Family Owned"> </dt>
			<dd>Family Owned</dd>
		</dl>
		<dl>
			<dt> <img src="public/images/content/why2.jpg" alt="24 - Hour Service"> </dt>
			<dd>24 - Hour Service</dd>
		</dl>
		<dl>
			<dt> <img src="public/images/content/why3.jpg" alt="Licensed and Insured"> </dt>
			<dd>Licensed and Insured</dd>
		</dl>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<h2>OUR GALLERY</h2>
		<div class="container">
			<img src="public/images/content/gallery1.jpg" alt="Gallery Image 1">
			<img src="public/images/content/gallery2.jpg" alt="Gallery Image 2">
			<img src="public/images/content/gallery3.jpg" alt="Gallery Image 3">
			<img src="public/images/content/gallery4.jpg" alt="Gallery Image 4">
			<img src="public/images/content/gallery5.jpg" alt="Gallery Image 5">
			<img src="public/images/content/gallery6.jpg" alt="Gallery Image 6">
			<img src="public/images/content/gallery7.jpg" alt="Gallery Image 7">
			<img src="public/images/content/gallery8.jpg" alt="Gallery Image 8">
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<h2>CONTACT US</h2>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="col-6 fl">
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
				<label><span class="ctc-hide">Phone</span>
					<input type="text" name="phone" placeholder="Phone:">
				</label>
				<label><span class="ctc-hide">Address</span>
					<input type="text" name="address" placeholder="Address:">
				</label>
				<div class="g-recaptcha"></div>
				<button type="submit" class="ctcBtn btn" disabled>SUBMIT</button>
				<br>
				<label>
					<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
			</div>
			<div class="col-6 fl">
				<label><span class="ctc-hide">Message</span>
					<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
				</label>
			</div>
			<div class="clearfix"></div>
		</form>
	</div>
</div>

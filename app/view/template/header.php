<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdLeft col-4 text-left fl">
					<a href="<?php echo URL; ?>"> <img src="public/images/common/mainLogo.png" alt="It Is What It Is Tree Service Main Logo"> </a>
				</div>
				<div class="hdRight col-8 text-uppercase text-right fr">
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
							<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
							<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
							<li <?php $this->helpers->isActiveMenu("testimonials"); ?>><a href="<?php echo URL ?>testimonials#content">TESTIMONIALS</a></li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
						</ul>
					</nav>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<h1>NEED ANY TREE WORK DONE <span>GRASS MOWER PAINTING DECKS</span> <span>GIVE US A CALL</span></h1>
				<p>We target the whole area of West Virginia and Tennessee</p>
				<a href="contact#content" class="btn">GET A QUOTE</a>
			</div>
			<div class="row">
				<div class="container">
					<h1>Tree Service in Bluff City, TN and Surrounding Areas <span>We are Available 24 Hours a Day 7 Days a Week</span></h1>
					<p>We are tree service company in Bluff City, TN, and our detail oriented staff will make sure that you and your property are taken care of. Our tree maintenance service provides the highest quality lawn care services whether it is a small apple tree prune or a large cottonwood removal.</p>
					<p>It Is What It Is Tree Service prides itself on giving our customers comprehensive information on your trees so that you may make a decision on what services your property needs. Our services include cautious tree removal and detailed tree pruning. With our stump grinding service, we can help you make the most of your property by shaping the natural landscape to your aesthetic preferences and safety standards.</p>
					<a href="services#content" class="btn">READ MORE</a>
					<img src="public/images/common/paint.png" alt="paint" class="paint">
				</div>
			</div>
		</div>
	<?php //endif; ?>
